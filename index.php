<?php

//GOOGLE

//Client ID: 85047344797-ersqkboch1rpt4gfo8eisvjtape94mvg.apps.googleusercontent.com

//Client secret: rFp74eIb57Nd-J_Bq-lzt9_a


//GITHUB

//Client ID: Iv1.e747d31d4808fbc2

//Client secret: a62ef6954fa6da689ef1cb9a489811ea68ea098b


//GOOGLE API

//Include Configuration File
include('Google API/config.php');

$login_button = '';

//This $_GET["code"] variable value received after user has login into their Google Account redirct to PHP script then this variable value has been received
if(isset($_GET["code"]))
{
 //It will Attempt to exchange a code for an valid authentication token.
 $token = $google_client->fetchAccessTokenWithAuthCode($_GET["code"]);

 //This condition will check there is any error occur during geting authentication token. If there is no any error occur then it will execute if block of code/
 if(!isset($token['error']))
 {
  //Set the access token used for requests
  $google_client->setAccessToken($token['access_token']);

  //Store "access_token" value in $_SESSION variable for future use.
  $_SESSION['access_token'] = $token['access_token'];

  //Create Object of Google Service OAuth 2 class
  $google_service = new Google_Service_Oauth2($google_client);

  //Get user profile data from google
  $data = $google_service->userinfo->get();

  //Below you can find Get profile data and store into $_SESSION variable
  if(!empty($data['given_name']))
  {
   $_SESSION['user_first_name'] = $data['given_name'];
  }

  if(!empty($data['family_name']))
  {
   $_SESSION['user_last_name'] = $data['family_name'];
  }

  if(!empty($data['email']))
  {
   $_SESSION['user_email_address'] = $data['email'];
  }
 }
}

//This is for check user has login into system by using Google account, if User not login into system then it will execute if block of code and make code for display Login link for Login using Google account.
if(!isset($_SESSION['access_token']))
{
 //Create a URL to obtain user authorization
 $login_button = '<a href="'.$google_client->createAuthUrl().'"><img src="Img/sign-in-with-google.png" /></a>';
}


// GITHUB API


// Include GitHub API config file
require_once 'Github API/gitConfig.php';

//Include and initialize user class
require_once 'Github API/User.class.php';
$user = new User();

if(isset($accessToken)){
    // Get the user profile info from Github
    $gitUser = $gitClient->apiRequest($accessToken);
    
    if(!empty($gitUser)){
        // User profile data
        $gitUserData = array();
        $gitUserData['oauth_provider'] = 'github';
        $gitUserData['oauth_uid'] = !empty($gitUser->id)?$gitUser->id:'';
        $gitUserData['name'] = !empty($gitUser->name)?$gitUser->name:'';
        $gitUserData['email'] = !empty($gitUser->email)?$gitUser->email:'';
        
        
        // Insert or update user data to the database
        $userData = $user->checkUser($gitUserData);
        
        // Put user data into the session
        $_SESSION['userData'] = $userData;
        
        // Render Github profile data
        $output  = 'Welcome User <br><br>';
        $output .= '<h3>Name: '.$userData['name'].'</h3>';
        $output .= '<h3>Email: '.$userData['email'].'</h3>';
        $output .= '<h3><a href="Github API/logoff.php">Logoff</a></h3>'; 
    }
    
}elseif(isset($_GET['code'])){
    // Verify the state matches the stored state
    if(!$_GET['state'] || $_SESSION['state'] != $_GET['state']) {
        header("Location: ".$_SERVER['PHP_SELF']);
    }
    
    // Exchange the auth code for a token
    $accessToken = $gitClient->getAccessToken($_GET['state'], $_GET['code']);
  
    $_SESSION['access_token'] = $accessToken;
  
    header('Location: ./');
}else{
    // Generate a random hash and store in the session for security
    $_SESSION['state'] = hash('sha256', microtime(TRUE) . rand() . $_SERVER['REMOTE_ADDR']);
    
    // Remove access token from the session
    unset($_SESSION['access_token']);
  
    // Get the URL to authorize
    $loginURL = $gitClient->getAuthorizeURL($_SESSION['state']);
    
    // Render Github login button
    $output = '<a href="'.htmlspecialchars($loginURL).'"><img src="Img/sign-in-with-github.png"></a>';
}
?>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Providers Test</title>
        <link rel="stylesheet" href="style.css"/>
    </head>

    <body class="body2">
        <table class="wrapper2">
            <tr>
                <td class="header">
                    <a class="logo" href="Home.php">Providers Test - Andrew MONDOR / Randy MBIYA - 3IW2</a>
                </td>
            </tr>

            <tr>
                <td class="content">
	                <center>
	                    <form id="formconnect" method="POST" action"">
	                        <h2>Sign Up</h2>
	                        </br></br>
	                        <table id="tableconnect">
                            <tr>
                              <td align="right">
                                <?php
                                	if($login_button == '')
                                	{
                                		echo 'Welcome User';
                                		echo '<br><br>';
                                		echo '<h3><b>Name :</b> '.$_SESSION['user_first_name'].' '.$_SESSION['user_last_name'].'</h3>';
                                		echo '<h3><b>Email :</b> '.$_SESSION['user_email_address'].'</h3>';
                                		echo '<h3><a href="Google API/logoff.php">Logout</h3></div>';
                                	}
                                	else
                                	{
                                		echo $login_button;
                                	}
                                ?>
                              </td>
                            </tr>

                            <tr>
                              <td align="right">
                                <?php echo $output; ?>
                              </td>
                            </tr>
	                        </table>
	                    </form>
	                </center>
                </td>
            </tr>
        </table>
    </body>
</html>