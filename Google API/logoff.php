<?php

include('config.php');

//Reset l'OAuth access token
$google_client->revokeToken();

//Detruire la session
session_destroy();

//Ramène vers l'index
header('location:../index.php');

?>